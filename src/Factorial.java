public class Factorial {
	public static void main(String[] args) {
		for (int count = 1; count <= 20; count++) {
			long  factorial = 1;
			for (int factor = 1; factor <= count; factor++) {
				factorial *= factor;
			}
			System.out.println(count + "! = " +factorial);
		}
	}
}

