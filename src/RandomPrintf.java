import java.util.Random;

public class RandomPrintf {
	public static void main(String[] args) {

		Random  dice = new Random();
		int roll = dice.nextInt(1, 7);
		System.out.printf("You rolled %06d!%n",roll);

		System.out.println("This costs €" + dice.nextDouble()*100 );
		System.out.printf("This costs €%09.2f" , dice.nextDouble()*100 );

	}
}