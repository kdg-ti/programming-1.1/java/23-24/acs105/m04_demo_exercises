package m04.ex06;

import java.util.Scanner;

public class CountSubstrings {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Please enter a string:");
		String examine = keyboard.nextLine();
		System.out.print("Please enter a string to search:");
		String search = keyboard.nextLine();
		int count = 0;
		int index = 0;
		boolean searching = true;
		//while (searching){
		while (searching){
			index=examine.indexOf(search,index);
			if (index == -1){
				searching = false;
			}else{
				count++;
				index++;
			}
		}
		System.out.printf("Found %s %d times ",search,count);


	}

}
